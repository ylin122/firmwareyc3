#ifndef YC_HIL_ATTITUDE_H_
#define YC_HIL_ATTITUDE_H_

#include <stdint.h>
#include "../uORB.h"
//by Yucong Lin
struct yc_hil_attitude_s {
  uint64_t timestamp;
  float roll;
  float pitch;
  float yaw;
  float rollspeed;
  float pitchspeed;
  float yawspeed;
};

ORB_DECLARE(yc_hil_attitude);
#endif
