#ifndef MULTI_ADSB_H_
#define MULTI_ADSB_H_

#include <stdint.h>
#include "../uORB.h"
//by Yucong Lin
struct multi_adsb_s {
 uint32_t addrs[5];
 float lats[5];
 float lons[5];
 float alts[5];
 float yaws[5];
 uint8_t number;
};

ORB_DECLARE(multi_adsb_message);
#endif
