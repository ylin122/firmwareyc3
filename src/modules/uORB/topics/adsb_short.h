#ifndef ADSB_SHORT_H_
#define ADSB_SHORT_H_

#include <stdint.h>
#include "../uORB.h"
//by Yucong Lin
struct adsb_short_s {
 uint32_t address;
 float latitude;
 float longtitude;
 float altitude;
 float h_velocity;
 float v_velocity;
 float heading;
};

ORB_DECLARE(adsb_message_short);
#endif
