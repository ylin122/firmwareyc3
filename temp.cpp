void
MavlinkReceiver::handle_message_yc_hil_attitude(mavlink_message_t *msg)
{
   mavlink_yc_hil_attitude_t hil_att;
   mavlink_msg_yc_hil_attitude_decode(msg, &hil_att);
   //assign to yc_hil_attitude_s
   struct yc_hil_attitude_s hil_att_s;
   hil_att_s.roll = hil_att.roll;
   hil_att_s.pitch = hil_att.pitch;
   hil_att_s.yaw = hil_att.yaw;
   hil_att_s.rollspeed = hil_att.rollspeed;
   hil_att_s.pitchspeed = hil_att.pitchspeed;
   hil_att_s.yawspeed = hil_att.yawspeed;
   //publish
   if(_yc_hil_attitude_pub <= 0){
     _yc_hil_attitude_pub = orb_advertise(ORB_ID(yc_hil_attitude),&hil_att_s);
   }
   else{
     orb_publish(ORB_ID(yc_hil_attitude),_yc_hil_attitude_pub,&hil_att_s);
   }
}//handle_message_yc_hil_attitude ends
